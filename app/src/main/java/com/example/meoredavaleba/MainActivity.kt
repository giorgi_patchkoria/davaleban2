package com.example.meoredavaleba

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth

class MainActivity : AppCompatActivity() {

    private lateinit var emailAddress: EditText
    private lateinit var password1: EditText
    private lateinit var password2: EditText
    private lateinit var registrationButton: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
        registrationlisteners()

    }

    private fun init() {
        emailAddress = findViewById(R.id.emailAddress)
        password1 = findViewById(R.id.password1)
        password2 = findViewById(R.id.password2)
        registrationButton = findViewById(R.id.registrationButton)

    }

    private fun registrationlisteners() {
        registrationButton.setOnClickListener {

            val email = emailAddress.text.toString()
            val password1 = password1.text.toString()
            val password2 = password2.text.toString()

            if(email.isEmpty() || password1.isEmpty() || password2.isEmpty()) {
                Toast.makeText(this, "empty", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }


            if(password1 == password2) {
                FirebaseAuth.getInstance()
                    .createUserWithEmailAndPassword(email, password1)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            Toast.makeText(this, "User has been registered successfully", Toast.LENGTH_LONG).show()
                        } else {
                            Toast.makeText(this, "error", Toast.LENGTH_SHORT).show()
                        }
                    }
            } else {
                Toast.makeText(this, "passwords must be same", Toast.LENGTH_SHORT).show()
            }

        }

    }
}